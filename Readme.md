#Usage:
This Python3 script can be used to collect CLI output from a Cisco IOS device and store the output in a file.

#Instructions


1. Clone this repo: https://chrhobbs@bitbucket.org/chrhobbs/ciscoiosgetcli.git
2. Install python module dependencies
3. Edit the ```ip_address.csv``` file with the list of IP/hostnames
4. Edit the ```print_output.py``` file with show commands you would like to collect
5. Run command:
```python3 print_output.py -c ip_addresses.csv > ./print_output.txt```
